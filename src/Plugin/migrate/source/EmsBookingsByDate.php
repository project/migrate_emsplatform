<?php

namespace Drupal\migrate_emsplatform\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;

/**
 * Source plugin for dynamically changing the URL to load a limited data set.
 *
 * @MigrateSource(
 *   id = "ems_booking_url_by_date"
 * )
 */
class EmsBookingsByDate extends Url {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['urls'])) {
      $configuration['urls'] = [$configuration['urls']];
    }

    // Limit the records to only showing records past today.
    $date = date('Y-m-d\TH:i:sP');
    foreach ($configuration['urls'] as &$url) {
      if (strpos($url, '?') !== FALSE) {
        $url .= '&';
      }
      else {
        $url .= '?';
      }
      $url .= 'minReserveStartTime=' . $date;

      // Limit the request to a certain number of records.
      $url .= '&pageSize=1000';
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

}
