<?php

namespace Drupal\migrate_emsplatform\Plugin\migrate_plus\authentication;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\AuthenticationPluginBase;
use GuzzleHttp\Client;
use Exception;

/**
 * Provides authentication for the EMS Platform endpoint.
 *
 * To set this migration up properly, a few configuration items are required.
 *
 * The "auth_host" will usually be the following:
 *   https://example.com/EmsPlatformServices/api/v1/clientauthentication
 *
 * The "urls" value for the feed will usually be something like the following:
 *   https://example.com/EmsPlatformServices/api/v1/buildings
 *   https://example.com/EmsPlatformServices/api/v1/rooms
 *
 * id: migrate_using_REST_Authentication
 * label: 'REST Authentication Plugin example'
 * migration_tags: null
 * migration_group: migrate_group
 * source:
 *   plugin: url
 *   # Specifies the http fetcher plugin.
 *   data_fetcher_plugin: http
 *   authentication:
 *     plugin: emsplatform_auth
 *     auth_host: xxx
 *     client_id: xxx
 *     secret: xxx
 *   headers:
 *     Accept: 'application/json; charset=utf-8'
 *     Content-Type: 'application/json'
 *   # One or more URLs from which to fetch the source data.
 *   urls: https:/...some.source
 *   # Specifies the JSON parser plugin.
 *   data_parser_plugin: json
 *   <<<<CONTINUES>>>>
 *
 * The auth credentials may also be stored in a global setting in the site's
 * settings.php file as follows:
 *
 * $settings['emsplatform_auth'] = [
 *   'client_id' => 'THE CLIENT ID',
 *   'secret' => 'THE SECRET',
 *   'auth_host' => 'https://example.com/.../clientauthentication',
 * ];
 *
 * @Authentication(
 *   id = "emsplatform_auth",
 *   title = @Translation("EMS Platform authorization token")
 * )
 */
class EmsPlatformAuth extends AuthenticationPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationOptions() {
    // Try loading the auth configuration from a settings line.
    $settings = Settings::get('emsplatform_auth', []);
    if (!empty($settings['client_id']) && !empty($settings['secret']) && !empty($settings['auth_host'])) {
      $client_id = $settings['client_id'];
      $secret = $settings['secret'];
      $auth_host = $settings['auth_host'];
    }
    // Try loading the auth configuration from the migration plugin.
    elseif (isset($this->configuration['client_id'], $this->configuration['secret'], $this->configuration['auth_host'])) {
      $client_id = $this->configuration['client_id'];
      $secret = $this->configuration['secret'];
      $auth_host = $this->configuration['auth_host'];
    }
    // If no auth conditions found fail.
    else {
      throw new MigrateException('The authentication "auth_host", "client_id" and "secret" values must be set for this connection.');
    }

    $client = new Client();
    $error = FALSE;
    try {
      $token_response = $client->request('POST', $auth_host, [
        'json' => [
          "clientId" => $client_id,
          "secret" => $secret,
        ],
      ]);
      if ($token_response->getStatusCode() == 200) {
        $token_response_array = json_decode($token_response->getBody(), TRUE);
      }
      else {
        $error = TRUE;
        $error_response = [
          'code' => $token_response->getStatusCode(),
          'reason' => $token_response->getReasonPhrase(),
          'payload' => json_decode($token_response->getBody(), TRUE),
        ];
        \Drupal::logger('migrate_plus')->error('Attempt to retrieve authorization token failed. Detail: <pre>@detail</pre>', [
          '@detail' => print_r($error_response),
        ]);
      }
    }
    catch (Exception $e) {
      $error = TRUE;
      watchdog_exception('error', $e, $e->getMessage());
    }

    if ($error || empty($token_response_array['clientToken'])) {
      throw new MigrateException('Error occurred while retrieving authorization token for client "' . $secret . '"');
    }

    return [
      'headers' => [
        'x-ems-api-token' => $token_response_array['clientToken'],
      ],
    ];
  }

}
