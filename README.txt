Migrate: EMS Platform
---------------------
This provides some example configuration files for importing data from EMS
Platform [1] into a Drupal site using the standard Migrate system in Drupal 8/9.


Requirements
--------------------------------------------------------------------------------
* Migrate Plus
  https://www.drupal.org/project/migrate_plus
* Migrate Tools
  https://www.drupal.org/project/migrate_tools


Authentication configuration
--------------------------------------------------------------------------------
The migration authentication is handled through the emsplatform_auth plugin,
which must be provided the appropriate client ID and secret values. This should
be handled through changes to the settings.php file, or a "secrets" file which
is loaded into the settings.php, using the following lines:

// Migrate: EMS Platform: authentication configuration.
// @see \Drupal\migrate_emsplatform\Plugin\migrate_plus\authentication\EmsPlatformAuth
$settings['emsplatform_auth'] = [
  'client_id' => 'the client ID',
  'secret' => 'the client secret',
  'auth_host' => 'https://exampleems.com/EmsPlatformServices/api/v1/clientauthentication',
];


Credits / contact
--------------------------------------------------------------------------------
Originally written by Mark Casias [2], Melissa Bent [3] and Damien McKenna [4].

Development is sponsored by Mediacurrent [5].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/migrate_emsplatform


References
--------------------------------------------------------------------------------
1: https://www.emssoftware.com
2: https://www.drupal.org/u/markie
3: https://www.drupal.org/u/merauluka
4: https://www.drupal.org/u/damienmckenna
5: https://www.mediacurrent.com
